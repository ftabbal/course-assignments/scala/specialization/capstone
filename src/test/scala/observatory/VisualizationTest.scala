package observatory

import org.junit.Assert._
import org.junit.Test

trait VisualizationTest extends MilestoneSuite {
  private val milestoneTest = namedMilestoneTest("raw data display", 2) _

  // Implement tests for the methods of the `Visualization` object

  @Test def testCalculateDistance(): Unit = {
    // expected distances used from https://www.movable-type.co.uk/scripts/latlong.html
    val locationDistances: Iterable[((Location, Location), Int)] = List(
      ((Location(0, 0), Location(0, 0)), 0), // same location, GMT Equator
      ((Location(25.5, 30.5), Location(25.5, 30.5)), 0), // same location, arbitrary
      ((Location(45.508888, -73.561668), Location(-45.508888, 106.438332)), 20015), // Antipodes, pi * earthRadius
      ((Location(-45.508888, 106.438332), Location(45.508888, -73.561668)), 20015), // Antipodes the other way, pi * earthRadius
      // Locations from https://www.latlong.net/place/brisbane-qld-australia-3337.html
      // and https://www.latlong.net/place/montreal-quebec-canada-27653.html
      ((Location(45.508888, -73.561668), Location(-27.470125, 153.021072)), 15473), // Distance between Montreal to Brisbane
      ((Location(-27.470125, 153.021072), Location(45.508888, -73.561668)), 15473), // Test that swapping locations return the same result
      ((Location(45.508888, -73.561668), Location(45.51, -73.56)), 1) // Two points with less that 1 km should return 1 km
    )

    for (p <- locationDistances) {
      val loc1 = p._1._1
      val loc2 = p._1._2
      val expected = p._2
      assertEquals(expected, scala.math.round(Visualization.distance(loc1, loc2)).toInt)
    }
  }

  @Test def testInterpolateColor(): Unit = {
    def areEqual(color1:Color, color2: Color): Boolean =
      color1.red == color2.red && color1.green == color2.green && color1.blue == color2.blue
    val defaultTemperatureColors: Iterable[(Temperature, Color)] =List(
      (60, Color(255, 255, 255)),
      (32, Color(255, 0, 0)),
      (12, Color(255, 255, 0)),
      (0, Color(0, 255, 255)),
      (-15, Color(0, 0, 255)),
      (-27, Color(255, 0, 255)),
      (-50, Color(33, 0, 107)),
      (-60, Color(0, 0, 0))
    )
    val temperatures: Iterable[(Temperature, Color)] =
      for (i <- -60 to 60) yield i.toDouble -> Visualization.interpolateColor(defaultTemperatureColors, i)

    // Check that default temperature colors are properly rendered
    defaultTemperatureColors.foreach({
      case (t, color) => temperatures.find(p => p._1 == t) match {
        case Some(p) => assertTrue(areEqual(color, p._2))
        case None =>
      }
    } )


  }

}
