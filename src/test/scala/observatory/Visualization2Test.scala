package observatory

import org.junit.Test

trait Visualization2Test extends MilestoneSuite {
  private val milestoneTest = namedMilestoneTest("value-added information visualization", 5) _

  @Test def testBilinearInterpolation(): Unit = {
    val expected = 146.1
    // Using value from the wikipedia article: https://en.wikipedia.org/wiki/Bilinear_interpolation#Example
    val actual = Visualization2.bilinearInterpolation(
      CellPoint(0.5, 0.2),91, 162, 210, 95)

    assert(expected == actual, s"Expected ${expected}, got ${actual}")
  }


}
