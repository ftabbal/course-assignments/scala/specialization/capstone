package observatory

import com.sksamuel.scrimage.{Image, Pixel}

import scala.math.{Pi, abs, acos, asin, cos, pow, round, sin, sqrt, toRadians}

/**
  * 2nd milestone: basic visualization
  */
object Visualization extends VisualizationInterface {
  val earthRadius: Double = 6371.0
  val p = 6

  private var temperatureColors: Map[Int, Color] = Map.empty[Int, Color]

  private def getColorForTemperature(temperature: Temperature, colorForTemperature: Iterable[(Temperature, Color)]): Color = {
    // Rounding temperature to 3 decimal places
    val intTemperature: Int = round(temperature * 1000).toInt // Storing it as a int
    temperatureColors.get(intTemperature) match {
      case Some(value) => value
      case None =>
        val color: Color = interpolateColor(colorForTemperature, temperature)
        temperatureColors = temperatureColors.updated(intTemperature, color)
        color
    }
  }

  def calculateDistanceHaversine(loc1: Location, loc2: Location): Double = {
    def haversine(angle: Double): Double = (1 - cos(angle)) / 2
    val φ1: Double = toRadians(loc1.lat)
    val φ2: Double = toRadians(loc2.lat)
    val λ1: Double = toRadians(loc1.lon)
    val λ2: Double = toRadians(loc2.lon)
    val hav = haversine(φ2 - φ1) + cos(φ1) * cos(φ2) * haversine(λ2 - λ1)

    2 * earthRadius * asin(sqrt(hav))
  }

  def distance(loc1: Location, loc2: Location): Double = {
    // Using https://en.wikipedia.org/wiki/Great-circle_distance with special cases as mentioned in the assignment
    def antipode(location: Location): Location = {
      val lat = -location.lat
      val long = 180 - abs(location.lon)
      if (location.lon > 0) Location(lat, -long)
      else Location(lat, long)
    }

    if (loc1 == loc2) return 0
    if (loc1 == antipode(loc2)) return Pi * earthRadius

//    val φ1: Double = toRadians(loc1.lat)
//    val φ2: Double = toRadians(loc2.lat)
//    val Δλ: Double = toRadians(abs(loc2.lon - loc1.lon))
//
//    val Δσ: Double = acos(sin(φ1) * sin(φ2) + cos(φ1) * cos(φ2) * cos(Δλ))
//
//    val result = earthRadius * Δσ
    val result = calculateDistanceHaversine(loc1, loc2)
    if (result < 1) 1 else result

  }

  /**
    * @param temperatures Known temperatures: pairs containing a location and the temperature at this location
    * @param location     Location where to predict the temperature
    * @return The predicted temperature at `location`
    */
  def predictTemperature(temperatures: Iterable[(Location, Temperature)], location: Location): Temperature = {
    // Using functions names from https://en.wikipedia.org/wiki/Inverse_distance_weighting

    def w(knownLoc: Location): Double = {
      val d = distance(knownLoc, location)
      1 / pow(d, p)
    }

    temperatures.find({ case (loc: Location, _) => distance(loc, location) <= 1 }) match {
      // Nearby (< 1km) location has been found
      case Some(value: (Location, Temperature)) => value._2
      case None => {
        val (temp, dist): (Double, Double) =
          temperatures.foldLeft((0.0, 0.0))(
            { case (acc: (Double, Double), (loc: Location, temp: Temperature)) =>
              val weight = w(loc)
              (acc._1 + weight * temp, acc._2 + weight)
            })
        temp / dist
      }
    }
  }

  /**
    * @param points Pairs containing a value and its associated color
    * @param value  The value to interpolate
    * @return The color that corresponds to `value`, according to the color scale defined by `points`
    */
  def interpolateColor(points: Iterable[(Temperature, Color)], value: Temperature): Color = {
    //val sortedPoints = points.toArray.sortBy(_._1)

    val min: (Temperature, Color) = points.minBy(p => p._1)
    val max: (Temperature, Color) = points.maxBy(p => p._1)

    if (value <= min._1) return min._2
    if (value >= max._1) return max._2

    def getClosestColors: ((Temperature, Color), (Temperature, Color)) = {
      var cold = (Double.MinValue, Color(0, 0, 0))
      var warm = (Double.MaxValue, Color(255, 255, 255))

      for (p <- points) {
        if (p._1 < value && p._1 > cold._1) cold = p
        if (p._1 > value && p._1 < warm._1) warm = p
      }
      (cold, warm)
    }

    points.find(p => p._1 == value ) match {
      case Some(value) => value._2
      case None =>
        val (cold, warm) = getClosestColors
        val temperature0 = cold._1
        val temperature1 = warm._1
        val color0 = cold._2
        val color1 = warm._2

        val t = (value - temperature0) / (temperature1 - temperature0)

        val r = round(color0.red + (color1.red - color0.red) * t).toInt
        val g = round(color0.green + (color1.green - color0.green) * t).toInt
        val b = round(color0.blue + (color1.blue - color0.blue) * t).toInt

        Color(r, g, b)
    }
  }

  /**
    * @param temperatures Known temperatures
    * @param colors       Color scale
    * @return A 360×180 image where each pixel shows the predicted temperature at its location
    */
  def visualize(temperatures: Iterable[(Location, Temperature)], colors: Iterable[(Temperature, Color)]): Image = {

    val width = 360
    val height = 180

    def convertPixel(x: Int, y: Int): Pixel = {
      val location = Location(y + 90, x)
      val temperature = predictTemperature(temperatures, location)
      val color = interpolateColor(colors, temperature)
      Pixel(color.red, color.green, color.blue, 255)
    }

    Image(width, height).map((x, y, _) => convertPixel(x, y))
  }

}

