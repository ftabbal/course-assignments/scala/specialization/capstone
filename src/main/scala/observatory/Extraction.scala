package observatory

import java.time.LocalDate
import scala.io.Source

/**
  * 1st milestone: data extraction
  */
object Extraction extends ExtractionInterface {

  // TODO: Add configuration to run Spark and use Dataframes or Datasets

  def getLocationsFromStations(stationFile: String): Map[(String, String), Location] = {
    Source.fromInputStream(getClass.getResourceAsStream(stationFile)).getLines()
      .map({
        s =>
          val row = s.split(",")
          if (row.size == 4)
            (row(0), row(1)) -> Location(row(2).toDouble, row(3).toDouble)
          else
            ("", "") -> Location(0, 0)
      })
      .filter({ case (p: (String, String), _) =>p._1.nonEmpty || p._2.nonEmpty }).toMap
  }

  def readTemperatureFile(temperatureFile: String, year: Year): Iterable[TemperatureInfo] = {
    def toCelsius(fahrenheit: Double) = (fahrenheit - 32) * 5 / 9
    val temperatureData = Source.fromInputStream(getClass.getResourceAsStream(temperatureFile), "utf-8").getLines()
    val temperatures =
      for (line <- temperatureData)
      yield line.split(",")
    temperatures.map(data =>
      TemperatureInfo(data(0), data(1),
        LocalDate.of(year, data(2).toInt, data(3).toInt),
        toCelsius(data(4).toDouble))
    ).filter(t => t.temperature < 9999.9).toList
  }

  /**
    * @param year             Year number
    * @param stationsFile     Path of the stations resource file to use (e.g. "/stations.csv")
    * @param temperaturesFile Path of the temperatures resource file to use (e.g. "/1975.csv")
    * @return A sequence containing triplets (date, location, temperature)
    */
  def locateTemperatures(year: Year, stationsFile: String, temperaturesFile: String): Iterable[(LocalDate, Location, Temperature)] = {
    val stations = getLocationsFromStations(stationsFile)
    val temperatures = readTemperatureFile(temperaturesFile, year)
    temperatures.map(t => stations.get((t.stationStn, t.stationWban)) match {
      case Some(value) => (t.date, value, t.temperature)
      case None => (LocalDate.MAX, Location(0, 0), 0.0)
    }).filter(p => p._1 != LocalDate.MAX)
  }

  /**
    * @param records A sequence containing triplets (date, location, temperature)
    * @return A sequence containing, for each location, the average temperature over the year.
    */
  def locationYearlyAverageRecords(records: Iterable[(LocalDate, Location, Temperature)]): Iterable[(Location, Temperature)] = {
    records
      .groupBy({ case (_, location, _) => location})
      .map({case (loc, entries) => (loc, entries.foldLeft(0.0)((acc, row) => acc + row._3) / entries.size)})
  }

  case class TemperatureInfo(stationStn: String, stationWban: String, date: LocalDate, temperature: Temperature)

}
