package observatory

import com.sksamuel.scrimage.{Image, Pixel, writer}
import observatory.Visualization.{interpolateColor, predictTemperature}

import java.io.File
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/**
  * 3rd milestone: interactive visualization
  */
object Interaction extends InteractionInterface {

  final private val alpha = 127 // value for title transparency
  final private val currentDirectory = new java.io.File(".").getCanonicalPath

  final private def makePath(year: Year, tile: Tile) = {
    s"$currentDirectory/target/temperatures/$year/${tile.zoom}/"
  }

  final private def makeFileName(tile: Tile): String = {
    s"${tile.x}-${tile.y}.png"
  }

  /**
    * @param tile Tile coordinates
    * @return The latitude and longitude of the top-left corner of the tile, as per http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    */
  def tileLocation(tile: Tile): Location = {
    Location(tile.lat, tile.lon)
  }

  /**
    * @param temperatures Known temperatures
    * @param colors Color scale
    * @param tile Tile coordinates
    * @return A 256×256 image showing the contents of the given tile
    */
  def tile(temperatures: Iterable[(Location, Temperature)], colors: Iterable[(Temperature, Color)], tile: Tile): Image = {
    def makePixel(location: Location): Pixel = {
      val temperature = predictTemperature(temperatures, location)
      val color = interpolateColor(colors, temperature)
      Pixel(color.red, color.green, color.blue, alpha)
    }

    val imgSide = 256
    val n = scala.math.pow(2, 8).toInt // zoom factor for each pixel

    val pixelArray: collection.mutable.ArraySeq[Pixel] = new collection.mutable.ArraySeq[Pixel](imgSide * imgSide)

    def generatePixel(arrayIndex: Int): Pixel = {
      val tileX = n * tile.x + arrayIndex % imgSide
      val tileY = n * tile.y + arrayIndex / imgSide
      val tileZoom: Int = 8 + tile.zoom
      val pixelTile = Tile(tileX, tileY, tileZoom)
      val location = tileLocation(pixelTile)
      makePixel(location)
    }

    def insterPixelsToArray(start: Int, end: Int): Unit = {
      for (i <- start until end)
        pixelArray(i) = generatePixel(i)
    }

    val taskArguments = for (start <- 0 until pixelArray.size by pixelArray.size / 4) yield (start, start + pixelArray.size / 4)

    val tasks = for ((start, end) <- taskArguments) yield Future {
      insterPixelsToArray(start, end)
    }

    tasks.foreach(t =>Await.ready(t, Duration.Inf))

    Image(imgSide, imgSide, pixelArray.toArray)

  }

  def write[Data]: (Year, Tile, Data) => Unit = (year: Year, aTile: Tile, data: Data) => data match {
    case value: Iterable[(Location, Temperature)] => {
      val path = makePath(year, aTile)
      val fileName = makeFileName(aTile)
      val img = tile(value, colorScale, aTile)
      new File(path).mkdirs()
      img.output(new File(path, fileName))
      println(s"${path + fileName} done!")
    }
    case _ => 
  }

  def makeTileList(maxZoom: Int): List[Tile] = {
    def prepareTiles(zoomLevel: Int): Iterable[Tile] =
      for (i <- 0 until scala.math.pow(2, zoomLevel).toInt; j <-0 until scala.math.pow(2, zoomLevel).toInt)
      yield Tile(i, j, zoomLevel)

    (for (i <- 0 to maxZoom) yield prepareTiles(i)).toList.flatten
  }

  /**
    * Generates all the tiles for zoom levels 0 to 3 (included), for all the given years.
    * @param yearlyData Sequence of (year, data), where `data` is some data associated with
    *                   `year`. The type of `data` can be anything.
    * @param generateImage Function that generates an image given a year, a zoom level, the x and
    *                      y coordinates of the tile and the data to build the image from
    */
  def generateTiles[Data](
    yearlyData: Iterable[(Year, Data)],
    generateImage: (Year, Tile, Data) => Unit
  ): Unit = {

    val tiles: List[Tile] = makeTileList(3)

//    val tilesByZoom = tiles.groupBy(_.zoom)
//    for (z <- tilesByZoom) {
//      println(s"zoom: ${z._1} => ${z._2.sortBy(_.y).sortBy(_.x).sortBy(_.zoom)}")
//    }

//    /*val syncTasks: Iterable[Unit] = */ for ((year, data) <- yearlyData) yield {
//      tiles.foreach(t => generateImage(year, t, data))
//    }

    val tasks = for ((year, data) <- yearlyData) yield Future {
      println(s"Generating tiles for year ${year}")
      tiles.foreach(t => generateImage(year, t, data))
    }

//    println(s"SyncTasks: ${syncTasks.size}")
//    println(s"asyncTasks: ${tasks.size}")

    tasks.foreach(t =>Await.ready(t, Duration.Inf))
  }

}
