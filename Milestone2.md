# Milestone 2 overview

This milestone consists in producing images showing the content of the temperature records. You will have to complete the file **Visualization.scala**. But first, remember to update the grading milestone number:

``` scala
val milestone: Int = 2
```

**Reminder**: You can’t get a 10/10 score an an individual milestone (rather only when all milestones are completed). The maximum grade you can get in this milestone is **4.89**.

Your records contain the average temperature over a year, for each station’s location. Your work consists in building an image of 360×180 pixels, where each pixel shows the temperature at its location. The point at latitude 0 and longitude 0 (the intersection between the Greenwich meridian and the equator) will be at the center of the image:

![](https://d3c33hcgiwev3.cloudfront.net/imageAssetProxy.v1/bU3Py4jfEeeesQolNF232g_2777597c8e677058efac69361b1b0c6c_mercator.svg?expiry=1628899200000&hmac=5J5MYAh-H3IQTnWo3TTex6SIvjcdMF2LPHPy49PmbKI)

In this figure, the red crosses represent the weather stations. As you can see, you will have to _spatially interpolate_ the data in order to guess the temperature corresponding to the location of each pixel (such a pixel is represented by a green square in the picture). Then you will have to convert this temperature value into a pixel color based on a color scale:

![](https://d3c33hcgiwev3.cloudfront.net/imageAssetProxy.v1/mo8MHIjfEeeesQolNF232g_a3004fca55a422b39511953290abe707_color-scale.png?expiry=1628899200000&hmac=ZZx_HrSHnnr3EBBrGITJtvprIjX8g4JMAY0784F8G5U)

This color scale means that a temperature of 60°C or above should be represented in white, a temperature of 32°C should be represented in red, a temperatures of 12°C should be represented in yellow, and so on. For temperatures between thresholds, say, between 12°C and 32°C, you will have to compute a _linear interpolation_ between the yellow and red colors.

Here are the RGB values of these colors:

|Temperature (°C)| Red |Green|Blue |
|:--------------:|:---:|:---:|:---:|
|       60       | 255 | 255 | 255 |
|       32       | 255 |  0  |  0  |
|       12       | 255 | 255 |  0  |
|        0       |  0  | 255 | 255 |
|      -15       |  0  |  0  | 255 |
|      -27       | 255 |  0  | 255 |
|      -50       | 33  |  0  | 107 |
|      -60       |  0  |  0  |  0  |

You can monitor your progress by submitting your work at any time during the development of this milestone. Your submission token and the list of your graded submissions is available on [this page](https://www.coursera.org/learn/scala-capstone/programming/NXfKi/scaffolding-material).

## Spatial interpolation

You will have to implement the following method:


``` scala
def predictTemperature(
  temperatures: Iterable[(Location, Temperature)],
  location: Location
): Temperature
```

This method takes a sequence of known temperatures at the given locations, and a location where we want to guess the temperature, and returns an estimate based on the [inverse distance weighting](https://en.wikipedia.org/wiki/Inverse_distance_weighting) algorithm (you can use any _p_ value greater or equal to 2; try and use whatever works best for you!). To approximate the distance between two locations, we suggest you to use the [great-circle distance](https://en.wikipedia.org/wiki/Great-circle_distance) formula.

Note that the great-circle distance formula is known to have rounding errors for short distances (a few meters), but that’s not a problem for us because we don’t need such a high degree of precision. Thus, you can use the first formula given on the Wikipedia page, expanded to cover some edge cases like equal locations and [antipodes](https://en.wikipedia.org/wiki/Antipodes#Mathematical_description):

![](https://d3c33hcgiwev3.cloudfront.net/imageAssetProxy.v1/G-FH04jgEeeb5Q5EoxCSaA_f91a8f5fc64e793cdc452075631b17f1_great-circle-angle-formula.svg?expiry=1628899200000&hmac=BTjjkD8Nk355TYh35arbmIBbobTCtFxrGrqVlralF1I)![](https://d3c33hcgiwev3.cloudfront.net/imageAssetProxy.v1/Jzuff4jgEee9xRJas1XV1A_8f51808a7dde2c0e0fabcc8f13f3e4bb_great-circle-distance-formula.svg?expiry=1628899200000&hmac=jcEgNYErwlhLKvUUBcS-6TqNbpiSsgAtZk8laokaPFc)

However, running the inverse distance weighting algorithm with small distances will result in huge numbers (since we divide by the distance raised to the power of p), which can be a problem. A solution to this problem is to directly use the known temperature of the close (less than 1 km) location as a prediction.

## Linear interpolation

We're providing you with a simple case class for representing color; you can see **Color**'s documentation in **models.scala** for more information.

``` scala
case class Color(red: Int, green: Int, blue: Int)
```

You will have to implement the following method:

``` scala
def interpolateColor(points: Iterable[(Temperature, Color)], value: Temperature): Color
```

This method takes a sequence of reference temperature values and their associated color, and a temperature value, and returns an estimate of the color corresponding to the given value, by applying a [linear interpolation](https://en.wikipedia.org/wiki/Linear_interpolation) algorithm.

Note that the given points are not sorted in a particular order.

## Visualization

Once you have completed the above steps you can implement the visualize method to build an image (using the [scrimage](https://index.scala-lang.org/sksamuel/scrimage) library) where each pixel shows the temperature corresponding to its location.

``` scala
def visualize(
  temperatures: Iterable[(Location, Temperature)],
  colors: Iterable[(Temperature, Color)]
): Image
```

Note that the (x,y) coordinates of the top-left pixel is (0,0) and then the x axis grows to the right and the y axis grows to the bottom, whereas the latitude and longitude origin, (0,0), is at the center of the image, and the top-left pixel has GPS coordinates (90, -180).

## Appendix: scrimage cheat sheet

Here is a description of scrimage’s API parts that are relevant for your work.
*   **Image** [type](https://static.javadoc.io/com.sksamuel.scrimage/scrimage-core_2.11/2.1.6/index.html#com.sksamuel.scrimage.Image) and [companion object](https://static.javadoc.io/com.sksamuel.scrimage/scrimage-core_2.11/2.1.6/index.html#com.sksamuel.scrimage.Image$).

*   A simple way to construct an image is to use constructors that take an **Array[Pixel]** as parameter. In such a case, the array must contain exactly width × height elements, in the following order: the first element is the top-left pixel, followed by all the pixels of the top row, followed by the other rows.

*   A simple way to construct a pixel from RGB values is to use [this constructor](https://static.javadoc.io/com.sksamuel.scrimage/scrimage-core_2.11/2.1.6/index.html#com.sksamuel.scrimage.Pixel$@apply(r:Int,g:Int,b:Int,alpha:Int):com.sksamuel.scrimage.Pixel).

*   To write an image into a PNG file, use the [**output**](https://static.javadoc.io/com.sksamuel.scrimage/scrimage-core_2.11/2.1.6/index.html#com.sksamuel.scrimage.Image@output(file:java.io.File)(implicitwriter:com.sksamuel.scrimage.nio.ImageWriter):java.io.File) method. For instance: **myImage.output(new java.io.File("target/some-image.png"))**.

*   To check that some predicate holds for all the pixels of an image, use the [**forall**](https://static.javadoc.io/com.sksamuel.scrimage/scrimage-core_2.11/2.1.6/index.html#com.sksamuel.scrimage.Image@forall(f:(Int,Int,com.sksamuel.scrimage.Pixel)=>Boolean):Boolean) method.

*   Also, note that scrimage defines a **Color** type, which could be ambiguous with our **Color** definition. Beware to not import scrimage’s **Color**.