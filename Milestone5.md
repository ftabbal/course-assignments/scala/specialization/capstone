# Milestone 5 overview

The goal of this milestone is to produce tile images from the grids generated at the previous milestone. You will have to complete the file Visualization2.scala. But first, remember to update the grading milestone number:

``` scala
val milestone: Int = 5
```

**Reminder**: You can’t get a 10/10 score an an individual milestone (rather only when all milestones are completed). The maximum grade you can get in this milestone is **8.89**.

As in the 3rd milestone, you will have to compute the color of every pixel of the tiles. But now the situation has changed: instead of working with a set of scattered points, you have a regular grid of points:

![](https://d3c33hcgiwev3.cloudfront.net/imageAssetProxy.v1/mZwkQojjEeeCsgo7n8L4tA_f9cbdb8e2f4694f414adf2986eadca46_bilinear-interpolation.svg?expiry=1629676800000&hmac=lwdhLhvA99S5BbwhocbibwZoZx_t8HCeqfXWTzQUeBQ)

In this figure, the square in the middle materializes a pixel that you want to compute. You can leverage the grid to use a faster interpolation algorithm: you can now use bilinear interpolation rather than inverse distance weighting.

More precisely, you will implement a simplified form of bilinear interpolation:

![](https://d3c33hcgiwev3.cloudfront.net/imageAssetProxy.v1/zjj8qPojEeetIhICvTHYsg_3467b9f610af7f7be66c2366ee148114_bilinear-interpolation-2.svg?expiry=1629676800000&hmac=RVmaOc11nlSbsEpO_aAZtbhJCfqH7We1iAnJo0EkcOk)

In this form of bilinear interpolation, the location of the point to estimate is given by coordinates x and y, which are numbers between 0 and 1\. The algorithm considers that the four known points, d00, d01, d10 and d11, form a _unit square_ whose origin is its top-left corner. As such, the coordinates of a pixel inside of a grid cell can be described by the following case class, defined in **models.scala**:

``` scala
case class CellPoint(x: Double, y: Double)
```

You will also have to decide on a color scale to use, to represent the temperature deviations. You can for instance use one like the following:

![](https://d3c33hcgiwev3.cloudfront.net/imageAssetProxy.v1/fJ0WRIjkEee5vBKArciZyA_79b4bd2ac2bcb263c8cd1777e727caef_deviations-colors.png?expiry=1629676800000&hmac=ZyzXSw6ogrBR-F9vCH1pzN8Uxi0knb-QblPJ9yFPYuI)

Here are the RGB values of these colors:

|Temperature (°C)| Red |Green|Blue |
|:--------------:|:---:|:---:|:---:|
|        7       |  0  |  0  |  0  |
|        4       | 255 |  0  |  0  |
|        2       | 255 | 255 |  0  |
|        0       | 255 | 255 | 255 |
|       -4       |  0  | 255 | 255 |
|       -7       |  0  |  0  | 255 |

You can monitor your progress by submitting your work at any time during the development of this milestone. Your submission token and the list of your graded submissions is available on [this page](https://www.coursera.org/learn/scala-capstone/programming/NXfKi/scaffolding-material).

## Visualization

You will have to implement the following methods:

``` scala
def bilinearInterpolation(
  point: CellPoint,
  d00: Temperature,
  d01: Temperature,
  d10: Temperature,
  d11: Temperature
): Temperature
```


This method takes the coordinates (**x** and **y** values between 0 and 1) of the location to estimate the temperature at, and the 4 known temperatures as shown in the above figure, and returns the estimated temperature at location **(x, y)**.

``` scala
def visualizeGrid(
  grid: GridLocation => Temperature,
  colors: Iterable[(Temperature, Color)],
  tile: Tile
): Image
```

This method takes a grid, a color scale and the coordinates of a tile, and returns the 256×256 image of this tile, where each pixel has a color computed according to the given color scale applied to the grid values.

_Hint:_ remember that our grid is a rectangular projection of a sphere, so **IndexOutOfBoundsExceptions** on coordinates should not be possible!

## Deviation tiles generation

Once you have implemented the above methods, you are ready to generate the tiles showing the deviations for all the years between 1990 and 2015, so that the final application (in the last milestone) will nicely display them:

*   Compute normals from yearly temperatures between 1975 and 1990 ;

*   Compute deviations for years between 1991 and 2015 ;

*   Generate tiles for zoom levels going from 0 to 3, showing the deviations. Use the output method of Image to write the tiles on your file system, under a location named according to the following scheme: **target/deviations/&lt;year&gt;/&lt;zoom&gt;/&lt;x&gt;-&lt;y&gt;.png**.

Note that this process is going to be very CPU consuming, or might even crash if your implementation tries to load too much data into memory. That being said, even a smart solution performing incremental data manipulation and parallel computations might take a lot of time (several days). You can reduce this time by using some of these ideas:

*   Identify which parts of the process are independent and perform them in parallel ;

*   Reduce the quality of the tiles. For instance, instead of computing 256×256 images, compute 128×128 images (that’s going to be 4 times fewer pixels to compute) and then scale them to fit the expected tile size ;

*   Reduce the quality of the spatial interpolation. For instance, instead of having grids with 360×180 points, you can use a grid with 120×60 points (that’s going to be 9 times fewer points to compute).